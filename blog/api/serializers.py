from rest_framework import serializers
from blog.models import Post
from users.models import Profile
from comments.models import Comment
from django.contrib.auth.models import User


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        exclude = ('id', 'birthday', 'user')


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = User
        exclude = ('id', 'password', 'is_superuser', 'last_login', 'is_staff', 'groups', 'user_permissions', 'date_joined', 'is_active')


class CommentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Comment
        exclude = ('id', 'post')


class PostSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)
    comments = CommentSerializer(many=True, read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'content', 'date_posted', 'author', 'comments')
