from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegistrationForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.models import User

def home_view(request):
    return render(request, 'home.html')

@login_required
def user_profile_view(request):
    user_update_form = UserUpdateForm(instance=request.user)
    profile_update_form = ProfileUpdateForm(instance=request.user.profile)

    if request.method == 'POST':
        user_update_form = UserUpdateForm(request.POST, instance=request.user)
        profile_update_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)

        if user_update_form.is_valid() and profile_update_form.is_valid():
            user_update_form.save()
            profile_update_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('user-profile')



    context = {
        'user_update_form': user_update_form,
        'profile_update_form': profile_update_form,
    }

    return render(request, 'users/profile.html', context)

def register_view(request):
    # check if POST request
    if request.method == "POST":
        form = UserRegistrationForm(request.POST)
        
        # check if data passed validation then display username
        if form.is_valid():
            form.save()
            messages.success(request, f'Your account has been created! Please login!')
            return redirect('login')

    else: # just render the form
        form = UserRegistrationForm()
    
    context = {
        'form': form        
    }

    return render(request, 'users/register.html', context)