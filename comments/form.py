from django import forms
from .models import Comment


class CommentForm(forms.ModelForm):
    content = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "placeholder": 'Write comment to this post...',
                "rows": 2
                }
            ), label='')

    class Meta:
        model = Comment
        fields = ['content']
