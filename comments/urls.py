from django.urls import path
from .views import delete_comment

urlpatterns = [
    path('comment/<int:pk>/delete', delete_comment, name="delete-comment"),
]