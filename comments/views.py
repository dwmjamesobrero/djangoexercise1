from django.shortcuts import render, get_object_or_404
from .models import Comment
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404

# from django.views.generic import DeleteView
# from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

def delete_comment(request, pk):
    # obj = get_object_or_404(Comment, id=pk)
    obj = Comment.objects.get(id = pk)

    if obj.user != request.user:
        raise Http404
    
    if request.method == 'POST':
        obj.delete()
        return HttpResponseRedirect(obj.get_absolute_url())
    
    context = {
        'object': obj
    }

    return render(request, 'comments/comment_confirm_delete.html', context)

# class CommentDeleteView(LoginRequiredMixin, DeleteView):
#     model = Comment
#     success_url = "/"
#     success_message = "Comment was deleted successfully."

#     def delete(self, request, *args, **kwargs):
#         messages.success(self.request, self.success_message)
#         return super().delete(request, *args, **kwargs)

#     def test_func(self):
#         comment = self.get_object()

#         if self.request.user == comment.author:
#             return True
#         return False